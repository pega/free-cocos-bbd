
var i = require('BarrierType')
cc.Class({
    extends: cc.Component,
    properties: function() {
        return {
            lbScore: {
                default: null,
                type: cc.Label
            },
            bType: {
                default: i.IsAddScore,
                type: i
            }
        }
    },
    start: function() {
        this.lbScore && (this.lbScore.node.rotation = -this.node.rotation), this.node.color = cc.color(200, this.randomNum(0, 255), this.randomNum(0, 255), 255), this.setScore(this.main.setBarrierScore())
    },
    randomNum: function(t, e) {
        var o = e - t,
            i = Math.random();
        return t + Math.floor(i * o)
    },
    setScore: function(t) {
        this.lbScore && (this.bType == i.IsHell && (t = this.main.balls.length < 20 ? Math.floor(this.main.balls.length / 10) + 3 : this.main.balls.length < 30 ? Math.floor(this.main.balls.length / 5) + 3 : Math.floor(this.main.balls.length / 3) + 3), this.score = t, this.lbScore.string = this.score.toString())
    },
    SubScore: function() {
        this.lbScore && (this.score -= 1, this.lbScore.string = this.score.toString())
    },
    onCollisionEnter: function(t, e) {
        if (cc.log("炸弹撞到了 ===== " + t.node.name), "Bomb" == t.node.name) {
            if (this.bType != i.IsBomb) return this.bType == i.IsCuty ? (cc.Mgr.AudioMgr.playSFX("newstage"), this.main.addBombExploreAtPos(this.node.position), this.main.addBallInGameIngFromCuty(this.node.position), void this.main.removeOneBarrier(this)) : this.bType == i.IsAddScore ? (this.main.addNumScore(this.score), void this.main.removeOneBarrier(this)) : void this.main.removeOneBarrier(this);
            cc.Mgr.AudioMgr.playSFX("bomb"), this.main.addBombExploreAtPos(this.node.position), this.main.removeOneBarrier(this)
        } else "lightFlash1" != t.node.name && "lightFlash2" != t.node.name || this.scheduleOnce(function() {
            this.main.removeOneBarrier(this)
        }, .2)
    },
    onBeginContact: function(t, e, o) {
        if (this.bType == i.IsAddBall) cc.Mgr.AudioMgr.playSFX("addone"), this.main.addBallInGameIng(this.node.position), this.main.removeOneBarrier(this);
        else if (this.bType == i.IsBomb) cc.Mgr.AudioMgr.playSFX("newstage"), this.main.addBombExploreAtPos(this.node.position), this.main.removeOneBarrier(this);
        else if (this.bType == i.IsCuty) cc.Mgr.AudioMgr.playSFX("newstage"), this.main.addBombExploreAtPos(this.node.position), this.main.addBallInGameIngFromCuty(this.node.position), this.main.removeOneBarrier(this);
        else if (this.bType == i.IsAddScore) cc.Mgr.AudioMgr.playSFX("block"), this.main.addScore(), 1 == this.score ? this.main.removeOneBarrier(this) : (this.SubScore(), this.main.shake(this));
        else if (this.bType == i.IsHell) {
            cc.Mgr.AudioMgr.playSFX("hammer"), this.main.addNumScore(10);
            var r = o.node.getComponent("Ball");
            null != r && this.main.hellSubBall(r), 1 == this.score ? this.main.removeOneBarrier(this) : (this.SubScore(), this.main.shake(this))
        }
    }
});