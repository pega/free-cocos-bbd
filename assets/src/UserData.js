
cc.Class({
    extends: cc.Component,
    properties: {
        highScore: 0,
        lv: 0
    },
    initData: function() {
        var t = cc.sys.localStorage.getItem("tan_userdata");
        if (null != t && "" != t) {
            console.log("干，有数据");
            var e = JSON.parse(t);
            this.highScore = e.highScore, this.lv = e.lv || 0, cc.log("数据 = " + JSON.stringify(t))
        } else {
            console.log("干，之前没有数据");
            var o = {};
            this.highScore = 0, this.lv = 0, o.highScore = 0, o.lv = 0, cc.sys.localStorage.setItem("tan_userdata", JSON.stringify(o))
        }
    },
    SaveData: function() {
        var t = {};
        t.lv = this.lv, t.highScore = this.highScore, cc.sys.localStorage.setItem("tan_userdata", JSON.stringify(t))
    },
    getHisTopScore: function() {
        return this.highScore
    }
});