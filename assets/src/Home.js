function i() {
    cc.Mgr = {}, cc.Mgr.InitAll = !1;
    var e = require('AudioMgr');
    cc.Mgr.AudioMgr = new e, cc.Mgr.AudioMgr.init(), cc.Mgr.Global = require('Global'), cc.Mgr.Global.InitData(), cc.Mgr.Utils = require('Utils');
    var o = require('UserData');
    cc.Mgr.UserData = new o, cc.Mgr.UserData.initData(), cc.Mgr.AdsMgr = require('AdsMgr'), cc.Mgr.AdsMgr.Init(), cc.Mgr.ShareInfos = require('ShareInfos'), cc.Mgr.ShareInfos.init(), cc.Mgr.PlatformController = require('PlatformController'), cc.Mgr.PlatformController.Init()
}
cc.director.initMgr = !1;
var r = require('RankPanel'),
    n = require('SetUI'),
    s = null;
cc.Class({
    extends: cc.Component,
    properties: {
        rankPanel: r,
        setUI: n,
        tipNode: cc.Node,
        progressBar: cc.ProgressBar,
        loadingLabel: cc.Label,
        startBtn: cc.Node,
        highScoreLbl: cc.Label,
        screenSpA: cc.Sprite,
        Atlas: cc.SpriteAtlas,
        luPingNode: cc.Node
    },
    onLoad: function() {
        s = this, 0 == cc.director.initMgr ? (i(), cc.Mgr.PlatformController.ShareTopNav(), cc.Mgr.AudioMgr.playBGM("bgm"), cc.director.initMgr = !0, cc.Mgr.PlatformController.CreateMoreGameBtn()) : cc.Mgr.PlatformController.ShowMoreGameBtn(), cc.Mgr.Global.sceneA = !0, cc.Mgr.PlatformController.setUserCloudStorage(cc.Mgr.UserData.getHisTopScore()), this.highScoreLbl.string = cc.Mgr.UserData.getHisTopScore(), cc.director.preloadScene("game", this.onProgressBarLv.bind(this), this.onLoadFinishScene.bind(this)), cc.director.on("ScreenOverA", function(t) {
            s.SetScreenSps()
        }), "tt" == cc.Mgr.PlatformController.platform || "swan" == cc.Mgr.PlatformController.platform ? this.luPingNode.active = !0 : this.luPingNode.active = !1
    },
    start: function() {
        this.startBtn.active = !0, cc.Mgr.Global.openAds && cc.Mgr.AdsMgr.RecoverShowBanner(), this.SetScreenSps()
    },
    SetScreenSps: function() {
        0 == cc.Mgr.PlatformController.IsScreenRecord ? this.screenSpA.spriteFrame = this.Atlas.getSpriteFrame("luping_off") : this.screenSpA.spriteFrame = this.Atlas.getSpriteFrame("luping_on")
    },
    ScreenRecord: function() {
        cc.Mgr.AudioMgr.playSFX("click"), 0 == cc.Mgr.PlatformController.IsScreenRecord ? (cc.Mgr.PlatformController.StartRecordScreen(), this.screenSpA.spriteFrame = this.Atlas.getSpriteFrame("luping_on")) : (cc.Mgr.PlatformController.StopRecordScreen(), this.screenSpA.spriteFrame = this.Atlas.getSpriteFrame("luping_off"))
    },
    onProgressBarLv: function(t, e, o) {
        var i = t / e;
        this.progressBar.progress = i, this.loadingLabel.string = "加载中...(" + parseInt(100 * i).toString() + ")"
    },
    onLoadFinishScene: function() {
        this.loadingLabel.string = "加载完成(100%)", this.startBtn.active = !0, this.progressBar.node.active = !1, this.loadingLabel.node.active = !1
    },
    gameStart: function() {
        cc.Mgr.PlatformController.HideMoreGameBtn(), cc.Mgr.AudioMgr.playSFX("click"), cc.Mgr.AdsMgr.HideBannerAd(), cc.director.loadScene("game")
    },
    JumpToApp: function(t, e) {
        cc.Mgr.PlatformController.JumpToOtherApp(e)
    },
    ShareGame: function() {
        cc.Mgr.AudioMgr.playSFX("click");
        var t = Math.floor(4 * Math.random());
        cc.Mgr.PlatformController.ShareToFriend(t)
    },
    OpenRankPanel: function() {
        cc.Mgr.AudioMgr.playSFX("click"), "tt" != cc.Mgr.PlatformController.platform ? (this.rankPanel.node.active = !0, cc.Mgr.PlatformController.SendMessageToSubView("RankPanelOpen")) : cc.Mgr.PlatformController.showToast("排行榜暂未开放")
    },
    OpenTipUI: function() {
        cc.Mgr.PlatformController.HideMoreGameBtn(), cc.Mgr.AudioMgr.playSFX("click"), this.tipNode.active = !0
    },
    CloseTipUI: function() {
        cc.Mgr.AudioMgr.playSFX("click"), this.tipNode.active = !1
    },
    OpenMenu: function() {
        cc.Mgr.PlatformController.HideMoreGameBtn(), cc.Mgr.AudioMgr.playSFX("click"), this.setUI.node.active = !0, this.setUI.ShowUI()
    }
})