cc.Class({
    extends: cc.Component,
    onLoad: function() {
        this.node.on("click", this.gameStart, this)
    },
    gameStart: function() {
        cc.director.loadScene("game")
    }
})