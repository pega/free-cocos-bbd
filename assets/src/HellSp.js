cc.Class({
    extends: cc.Component,
    properties: {},
    getRandomNum: function(t, e) {
        var o = e - t,
            i = Math.random();
        return t + Math.floor(i * o)
    },
    start: function() {
        this.node.color = cc.color(200, this.getRandomNum(0, 255), this.getRandomNum(0, 255), 255)
    }
});