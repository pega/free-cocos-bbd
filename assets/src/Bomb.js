cc.Class({
    extends: cc.Component,
    properties: {
        anima: cc.Animation
    },
    onLoad: function() {
        this.node.scale = cc.v2(cc.Mgr.Global.boomScale, cc.Mgr.Global.boomScale)
    },
    PlayAni: function() {
        this.anima.play("bomb")
    },
    ExploreEnd: function() {
        this.node.destroy()
    }
});