
var i = require('Config')
cc.Class({
    extends: cc.Component,
    properties: function() {
        return {
            rigidBody: {
                type: cc.RigidBody,
                default: null
            },
            isTouchedGround: !1
        }
    },
    onLoad: function() {
        var t = cc.winSize;
        this.winWidth = t.width, this.winHeight = t.height, this.rigidBody = this.getComponent(cc.RigidBody), this.collider = this.getComponent(cc.Collider)
    },
    update: function(t) {
        if (this.isTouchedGround) {
            this.rigidBody.active = !1, this.rigidBody.linearVelocity = cc.Vec2.ZERO;
            var e = [];
            e.push(this.node.position), this.node.position.x > 0 ? (e.push(cc.v2(this.winWidth / 2 - 30, -360)), e.push(cc.v2(this.winWidth / 2 - 30, 615)), e.push(cc.v2(162, 560))) : (e.push(cc.v2(-this.winWidth / 2 + 30, -360)), e.push(cc.v2(-this.winWidth / 2 + 30, 615)), e.push(cc.v2(-162, 560))), this.node.runAction(cc.sequence(cc.cardinalSplineTo(1, e, .5), cc.callFunc(function() {
                this.rigidBody.active = !0, this.node.group = i.groupBallInRecycle, this.main.recycleOneShootOutBall()
            }.bind(this)))), this.isTouchedGround = !1
        }
    },
    onBeginContact: function(t, e, o) {
        "ground" == o.node.name && (this.isTouchedGround = !0, cc.Mgr.AudioMgr.playSFX("undo"), cc.Mgr.PlatformController.QuakeScreen(!1))
    }
});