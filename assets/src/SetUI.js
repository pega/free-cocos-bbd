cc.Class({
    extends: cc.Component,
    properties: {
        Atlas: cc.SpriteAtlas,
        mSp: cc.Sprite,
        vSp: cc.Sprite
    },
    ShowUI: function(t) {
        1 == cc.Mgr.AudioMgr.musicState ? this.mSp.spriteFrame = this.Atlas.getSpriteFrame("music_on") : this.mSp.spriteFrame = this.Atlas.getSpriteFrame("music_off"), 1 == cc.Mgr.AudioMgr.voiceState ? this.vSp.spriteFrame = this.Atlas.getSpriteFrame("voice_on") : this.vSp.spriteFrame = this.Atlas.getSpriteFrame("voice_off")
    },
    ChangeStateM: function() {
        1 == cc.Mgr.AudioMgr.musicState ? (this.mSp.spriteFrame = this.Atlas.getSpriteFrame("music_off"), cc.Mgr.AudioMgr.pauseMusic()) : (this.mSp.spriteFrame = this.Atlas.getSpriteFrame("music_on"), cc.Mgr.AudioMgr.resumeMusic())
    },
    ChangeStateV: function() {
        1 == cc.Mgr.AudioMgr.voiceState ? (this.vSp.spriteFrame = this.Atlas.getSpriteFrame("voice_off"), cc.Mgr.AudioMgr.pauseVoice()) : (this.vSp.spriteFrame = this.Atlas.getSpriteFrame("voice_on"), cc.Mgr.AudioMgr.resumeVoice())
    },
    CloseUI: function() {
        cc.Mgr.PlatformController.ShowMoreGameBtn(), this.node.active = !1
    }
});