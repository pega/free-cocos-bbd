cc.Class({
    extends: cc.Component,
    statics: {
        FormatNumToTime: function(t) {
            var e = Math.floor(t / 3600),
                o = Math.floor((t - 3600 * e) / 60),
                i = Math.floor(t - 3600 * e - 60 * o),
                r = e,
                n = o,
                s = i;
            return e < 10 && (r = "0" + e), o < 10 && (n = "0" + o), i < 10 && (s = "0" + i), r + ":" + n + ":" + s
        },
        GetSysTime: function() {
            return Math.round(new Date / 1e3) - 1545264e3
        }
    }
});