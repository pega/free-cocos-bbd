
var i = null
cc.Class({
    extends: cc.Component,
    properties: {
        bgmVolume: .5,
        sfxVolume: .5,
        bgmAudioID: -1,
        sfxAudioID: -1,
        musicState: 1,
        voiceState: 1
    },
    init: function() {
        var t;
        i = this, cc.Mgr.loadSound = !1, null != (t = cc.sys.localStorage.getItem("bgmVolume")) && (this.bgmVolume = parseFloat(t)), null != (t = cc.sys.localStorage.getItem("sfxVolume")) && (this.sfxVolume = parseFloat(t)), cc.game.on(cc.game.EVENT_HIDE, function() {
            console.log("cc.audioEngine.pauseAll"), cc.audioEngine.pauseAll()
        }), cc.game.on(cc.game.EVENT_SHOW, function() {
            console.log("cc.audioEngine.resumeAll"), 1 == i.musicState ? i.resumeAll() : i.pauseAll()
        })
    },
    getUrl: function(t) {
        return cc.url.raw("resources/sound/" + t + ".mp3")
    },
    playBGM: function(t) {
        var e = this.getUrl(t);
        this.bgmAudioID >= 0 && cc.audioEngine.stop(this.bgmAudioID), this.bgmAudioID = cc.audioEngine.play(e, !0, this.bgmVolume)
    },
    playSFX: function(t) {
        if (0 != this.voiceState) {
            var e = this.getUrl(t);
            cc.audioEngine.play(e, !1, this.sfxVolume)
        }
    },
    setSFXVolume: function(t) {
        this.sfxVolume != t && (cc.sys.localStorage.setItem("sfxVolume", t), this.sfxVolume = t)
    },
    setBGMVolume: function(t, e) {
        this.bgmAudioID >= 0 && (t > 0 ? cc.audioEngine.resume(this.bgmAudioID) : cc.audioEngine.pause(this.bgmAudioID)), (this.bgmVolume != t || e) && (cc.sys.localStorage.setItem("bgmVolume", t), this.bgmVolume = t, cc.audioEngine.setVolume(this.bgmAudioID, t))
    },
    pauseAll: function() {
        this.musicState = 0, this.voiceState = 0, this.bgmVolume = 0, this.sfxVolume = 0, cc.audioEngine.pauseAll()
    },
    resumeAll: function() {
        this.musicState = 1, this.voiceState = 1, this.bgmVolume = .5, this.sfxVolume = .5, cc.audioEngine.resumeAll()
    },
    pauseMusic: function() {
        this.musicState = 0, this.bgmVolume = 0, this.bgmAudioID >= 0 && cc.audioEngine.pause(this.bgmAudioID)
    },
    resumeMusic: function() {
        this.musicState = 1, this.bgmVolume = .5, this.bgmAudioID >= 0 && cc.audioEngine.resume(this.bgmAudioID)
    },
    pauseVoice: function() {
        this.voiceState = 0, this.sfxVolume = 0
    },
    resumeVoice: function() {
        this.voiceState = 1, this.sfxVolume = .5
    },
    getMusicState: function() {
        return this.musicState
    },
    getVoiceState: function() {
        return this.voiceState
    }
});