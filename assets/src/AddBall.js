
var i = require('MainController')
cc.Class({
    extends: cc.Component,
    properties: {
        main: i
    },
    shake: function() {
        var t = cc.shake(.7, 1, 1);
        this.node.runAction(t)
    },
    onBeginContact: function(t, e, o) {
        "ball" == o.node.name && (cc.Mgr.AudioMgr.playSFX("block"), this.main.addScore(), cc.Mgr.PlatformController.QuakeScreen(!1))
    }
});