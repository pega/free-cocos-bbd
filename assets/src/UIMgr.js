var i = null
cc.Class({
    extends: cc.Component,
    properties: {
        tipNode: cc.Node,
        screenSpB: cc.Sprite,
        Atlas: cc.SpriteAtlas,
        luPingNode: cc.Node
    },
    start: function() {
        i = this, cc.director.on("ScreenOverB", function(t) {
            i.SetScreenSps()
        }), this.SetScreenSps(), "tt" == cc.Mgr.PlatformController.platform || "swan" == cc.Mgr.PlatformController.platform ? this.luPingNode.active = !0 : this.luPingNode.active = !1
    },
    SetScreenSps: function() {
        0 == cc.Mgr.PlatformController.IsScreenRecord ? this.screenSpB.spriteFrame = this.Atlas.getSpriteFrame("luping_off") : this.screenSpB.spriteFrame = this.Atlas.getSpriteFrame("luping_on")
    },
    ScreenRecord: function() {
        cc.Mgr.AudioMgr.playSFX("click"), 0 == cc.Mgr.PlatformController.IsScreenRecord ? (cc.Mgr.PlatformController.StartRecordScreen(), this.screenSpB.spriteFrame = this.Atlas.getSpriteFrame("luping_on")) : (cc.Mgr.PlatformController.StopRecordScreen(), this.screenSpB.spriteFrame = this.Atlas.getSpriteFrame("luping_off"))
    },
    OpenTipUI: function() {
        cc.Mgr.AudioMgr.playSFX("click"), this.tipNode.active = !0, cc.Mgr.Global.openAds && cc.Mgr.AdsMgr.RecoverShowBanner()
    },
    CloseTipUI: function() {
        cc.Mgr.AudioMgr.playSFX("click"), this.tipNode.active = !1, cc.Mgr.AdsMgr.HideBannerAd()
    }
});