
cc.Shake = cc.ActionInterval.extend({
    _initial_x: 0,
    _initial_y: 0,
    _strength_x: 0,
    _strength_y: 0,
    ctor: function(t, e, o) {
        this.initWithDuration(t, e, o)
    },
    initWithDuration: function(t, e, o) {
        return cc.ActionInterval.prototype.initWithDuration.call(this, t), this._strength_x = e, this._strength_y = o, !0
    },
    rangeRand: function(t, e) {
        return Math.random() * (e - t) + t
    },
    update: function() {
        var t = this.rangeRand(-this._strength_x, this._strength_x),
            e = this.rangeRand(-this._strength_y, this._strength_y);
        this.getTarget().setPosition(t + this._initial_x, e + this._initial_y)
    },
    startWithTarget: function(t) {
        cc.ActionInterval.prototype.startWithTarget.call(this, t), this._initial_x = t.x, this._initial_y = t.y
    },
    stop: function() {
        this.getTarget().setPosition(new cc.Vec2(this._initial_x, this._initial_y)), cc.ActionInterval.prototype.stop.call(this)
    }
})


cc.shake = function(t, e, o) {
    return new cc.Shake(t, e, o)
}