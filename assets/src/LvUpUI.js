cc.Class({
    extends: cc.Component,
    properties: {
        DesLbl: cc.Label,
        paticle: cc.ParticleSystem
    },
    ShowUI: function() {
        cc.Mgr.UserData.lv += 1, this.DesLbl.string = "恭喜你成功闯关\n第" + cc.Mgr.UserData.lv + "级", this.paticle.node.active = !0, this.paticle.resetSystem()
    },
    CloseUI: function() {
        this.paticle.node.active = !1, this.node.active = !1
    }
});