cc.Class({
    extends: cc.Component,
    properties: function() {
        return {
            arraw: cc.Sprite
        }
    },
    onLoad: function() {
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this)
    },
    onTouchMove: function(t) {
        if (this.node.main.isRecycleBallsFinished()) {
            var e = cc.v2(0, 446),
                o = this.node.convertTouchToNodeSpaceAR(t.touch);
            if (!(o.y > e.y)) {
                var i = this.node.getComponent(cc.Graphics),
                    r = o.sub(e),
                    n = r.mag(),
                    s = r.normalize().mul(40),
                    c = e.clone();
                for (i.fillColor = cc.color(255, 255, 255, 150), c.addSelf(s), c.addSelf(s), i.clear(); n > 40;) i.circle(c.x, c.y, 5), i.fill(), c.addSelf(s), n -= 40;
                var a = e.sub(o),
                    l = Math.atan2(a.y, a.x) / Math.PI * 180;
                this.arraw.node.rotation = -l
            }
        }
    }
})