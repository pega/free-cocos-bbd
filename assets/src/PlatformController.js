

var i = null
cc.Class({
    statics: {
        IsScreenRecord: !1,
        wxSubContextViewLock: 0,
        platform: "pc",
        Init: function() {
            window.wx || window.tt || window.swan || (this.platform = "pc"), i = this
        },
        IsLoginSync: function() {
            return "baidu" != this.platform || !!swan.isLoginSync().isLogin && (console.log("已经有登陆了"), !0)
        },
        CheckTTSession: function(t) {
            tt.checkSession({
                success: function(e) {
                    t(!0), console.log("session未过期")
                },
                fail: function(e) {
                    console.log("session已过期，需要重新登录"), t(!1)
                }
            })
        },
        Login: function() {
            "baidu" == this.platform ? swan.login({
                success: function() {
                    i.setUserCloudStorage(cc.Mgr.UserData.getHisTopScore())
                },
                fail: function() {
                    swan.showModal({
                        title: "登录失败",
                        content: "是否重新登录？",
                        cancelText: "退出游戏",
                        success: function(t) {
                            t.confirm ? (console.log("点击了确定"), i.Login()) : t.cancel && (console.log("点击了取消"), swan.exit())
                        }
                    })
                }
            }) : "tt" == this.platform && tt.login({
                success: function() {
                    i.setUserCloudStorage(cc.Mgr.UserDataMgr.HistoryHighAssets)
                },
                fail: function() {
                    tt.showModal({
                        title: "登录失败",
                        content: "是否重新登录？",
                        cancelText: "退出游戏",
                        success: function(t) {
                            t.confirm ? (console.log("点击了确定"), i.Login()) : t.cancel && (console.log("点击了取消"), tt.exitMiniProgram({}))
                        }
                    })
                }
            })
        },
        CreateMoreGameBtn: function() {
            "tt" == this.platform && (this.ttMoreGameBtn = tt.createMoreGamesButton({
                type: "image",
                image: "images/moregamebtn.png",
                style: {
                    left: 20,
                    top: 260,
                    width: 45,
                    height: 60,
                    lineHeight: 40,
                    backgroundColor: "#ffffff",
                    textColor: "#ffffff",
                    textAlign: "center",
                    fontSize: 16,
                    borderRadius: 4,
                    borderWidth: 1,
                    borderColor: "#ffffff"
                },
                appLaunchOptions: [{
                    appId: "tt39254f98a60fd13b"
                }, {
                    appId: "tt18ba6e078790f9a1"
                }, {
                    appId: "tt62dc1461fdf688b4"
                }, {
                    appId: "tt68a31e1ef78989bb"
                }, {
                    appId: "ttf14d87f06e671d2f"
                }, {
                    appId: "tta91adfa7363051b9"
                }, {
                    appId: "tt5ced2a50c3c7bbba"
                }, {
                    appId: "tt9bd2ed6f04f82beb"
                }, {
                    appId: "tt5e6228dfc148d0ae"
                }, {
                    appId: "ttbee0c85d21ca306a"
                }],
                onNavigateToMiniGame: function(t) {
                    console.log("跳转其他小游戏", t)
                }
            }), this.ttMoreGameBtn.onTap(function() {
                console.log("点击更多游戏")
            }))
        },
        ShowMoreGameBtn: function() {
            "tt" == this.platform && null != this.ttMoreGameBtn && this.ttMoreGameBtn.show()
        },
        HideMoreGameBtn: function() {
            "tt" == this.platform && null != this.ttMoreGameBtn && this.ttMoreGameBtn.hide()
        },
        CreateGameClub: function() {
            if ("wx" == this.platform) {
                var t = cc.view.getVisibleSize(),
                    e = 68 / t.width,
                    o = 275 / t.height,
                    r = wx.getSystemInfoSync();
                this.leftPos = r.windowWidth * e, this.topPos = r.windowHeight * o, console.log(this.topPos + "  ================创建游戏圈按钮===============" + this.leftPos), this.gameClubBtn = wx.createGameClubButton({
                    icon: "green",
                    text: "游戏圈",
                    style: {
                        left: i.leftPos - 20,
                        top: i.topPos - 20,
                        width: 45,
                        height: 45
                    }
                })
            }
        },
        ShowClubButton: function() {
            var t = arguments.length > 0 && void 0 !== arguments[0] && arguments[0];
            "wx" == this.platform && null != this.gameClubBtn && (1 == t ? this.gameClubBtn.show() : this.gameClubBtn.hide())
        },
        ShareTopNav: function() {
            var t = Math.floor(4 * Math.random()),
                e = cc.Mgr.ShareInfos.getShareInfos(t);
            "wx" == this.platform ? (wx.showShareMenu({
                withShareTicket: !0
            }), wx.onShareAppMessage(function() {
                return {
                    title: e.text,
                    imageUrl: e.url
                }
            })) : "tt" == this.platform ? (tt.showShareMenu(!1), tt.onShareAppMessage(function() {
                return {
                    title: e.text,
                    imageUrl: e.url
                }
            })) : "baidu" == this.platform && (swan.showShareMenu(!1), swan.onShareAppMessage(function() {
                return {
                    title: e.text,
                    imageUrl: e.url
                }
            }))
        },
        ShareToFriendTxt: function(t) {
            var e = Math.floor(4 * Math.random()),
                o = cc.Mgr.ShareInfos.getShareInfos(e);
            "wx" == this.platform ? (console.log("点击了分享啊"), wx.shareAppMessage({
                title: t,
                imageUrl: o.url
            })) : "tt" == this.platform ? tt.shareAppMessage({
                title: t,
                imageUrl: o.url
            }) : "baidu" == this.platform && (console.log("点击了分享啊"), swan.shareAppMessage({
                title: t,
                imageUrl: o.url
            }))
        },
        ShareToFriend: function(t) {
            var e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : null;
            this.cb = null, this.cb = e;
            var o = cc.Mgr.ShareInfos.getShareInfos(t);
            "wx" == this.platform ? (console.log("点击了分享啊"), wx.shareAppMessage({
                title: o.text,
                imageUrl: o.url
            })) : "tt" == this.platform ? tt.shareAppMessage({
                title: o.text,
                imageUrl: o.url,
                success: function() {
                    null != i.cb && i.cb(0)
                },
                fail: function(t) {
                    console.log("分享视频失败")
                }
            }) : "baidu" == this.platform && (console.log("点击了分享啊"), swan.shareAppMessage({
                title: o.text,
                imageUrl: o.url,
                success: function() {
                    null != i.cb && i.cb(0)
                },
                fail: function(t) {
                    console.log("分享视频失败")
                }
            }))
        },
        showToast: function(t) {
            cc.log(t), "wx" == this.platform ? wx.showToast({
                title: t,
                icon: "none",
                duration: 2e3
            }) : "tt" == this.platform ? tt.showToast({
                title: t,
                icon: "none",
                duration: 2e3
            }) : "baidu" == this.platform && swan.showToast({
                title: t,
                icon: "none",
                duration: 2e3
            })
        },
        setUserCloudStorage: function(t) {
            var e, o;
            (console.log("setUserCloudStorage socre = " + t), "wx" == this.platform) && ((e = {
                wxgame: {}
            }).wxgame.score = t, e.wxgame.update_time = (new Date).getTime(), console.log(JSON.stringify(e)), (o = new Array).push({
                key: "\bjzScore",
                value: JSON.stringify(e)
            }), wx.setUserCloudStorage({
                KVDataList: o,
                success: function(t) {
                    console.log("success:" + JSON.stringify(t))
                },
                fail: function(t) {
                    console.log("fail : " + t)
                }
            }));
            "baidu" == this.platform && ((e = {
                wxgame: {}
            }).wxgame.score = t, e.wxgame.update_time = (new Date).getTime(), console.log(JSON.stringify(e)), (o = new Array).push({
                key: "\bjzScore",
                value: JSON.stringify(e)
            }), swan.setUserCloudStorage({
                KVDataList: o,
                success: function(t) {
                    console.log("success:" + JSON.stringify(t))
                },
                fail: function(t) {
                    console.log("fail : " + t)
                }
            }))
        },
        IsSupportRank: function() {
            return "wx" == this.platform
        },
        showSubContentView: function() {
            "wx" == this.platform && (this.wxSubContextViewLock++, console.log("showSubContentView   " + this.wxSubContextViewLock))
        },
        hideSubContentView: function() {
            "wx" == this.platform && (this.wxSubContextViewLock--, console.log("hideSubContentView   " + this.wxSubContextViewLock))
        },
        SendMessageToSubView: function(t) {
            var e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 0,
                o = {};
            o.code = t, o.curScore = e, "wx" == this.platform ? wx.getOpenDataContext().postMessage({
                message: o
            }) : "baidu" == this.platform && swan.getOpenDataContext().postMessage({
                message: o
            })
        },
        QuakeScreen: function() {
            var t = !(arguments.length > 0 && void 0 !== arguments[0]) || arguments[0];
            "pc" != this.platform && (1 == t ? "tt" == this.platform ? tt.vibrateLong({
                success: function(t) {
                    console.log("" + t)
                },
                fail: function(t) {
                    console.log("vibrateLong调用失败")
                }
            }) : "baidu" == this.platform ? swan.vibrateLong({
                success: function(t) {
                    console.log("" + t)
                },
                fail: function(t) {
                    console.log("vibrateLong调用失败")
                }
            }) : "wx" == this.platform && wx.vibrateLong({
                success: function(t) {
                    console.log("" + t)
                },
                fail: function(t) {
                    console.log("vibrateLong调用失败")
                }
            }) : "tt" == this.platform ? tt.vibrateShort({
                success: function(t) {
                    console.log("" + t)
                },
                fail: function(t) {
                    console.log("vibrateShort调用失败")
                }
            }) : "baidu" == this.platform ? swan.vibrateShort({
                success: function(t) {
                    console.log("" + t)
                },
                fail: function(t) {
                    console.log("vibrateShort调用失败")
                }
            }) : "wx" == this.platform && wx.vibrateShort({
                success: function(t) {
                    console.log("" + t)
                },
                fail: function(t) {
                    console.log("vibrateLong调用失败")
                }
            }))
        },
        IsSupportRecordScreen: function() {
            return "tt" == this.platform || "baidu" == this.platform && !!this.CompareSdkVesrion(swan.getSystemInfoSync().SDKVersion, "1.4.1")
        },
        CompareSdkVesrion: function(t, e) {
            for (var o = t.split("."), i = e.split("."), r = 0; r < o.length; r++) {
                if (parseInt(o[r]) < parseInt(i[r])) return 0;
                if (parseInt(o[r]) > parseInt(i[r])) return 1
            }
            return 1
        },
        StartRecordScreen: function() {
            var t = !(arguments.length > 0 && void 0 !== arguments[0]) || arguments[0];
            this.videoPath = "", this.IsScreenRecord = !0, this.overShare = t, this.showToast("开始录制"), cc.Mgr.Global.screenStartTime = cc.Mgr.Utils.GetSysTime(), "tt" == this.platform ? (null == this.recorderManager && (this.recorderManager = tt.getGameRecorderManager(), this.recorderManager.onStart(function(t) {
                console.log("录制开始了: " + t)
            }), this.recorderManager.onStop(function(t) {
                console.log("录制结束了: " + t.videoPath), i.IsScreenRecord = !1, 1 == cc.Mgr.Global.sceneA ? cc.director.emit("ScreenOverA", {}) : cc.director.emit("ScreenOverB", {}), i.videoPath = t.videoPath, 1 == i.overShare ? i.ShareRecordScreen() : i.showToast("结束录屏")
            }), this.recorderManager.onPause(function() {
                console.log("录制暂停了")
            }), this.recorderManager.onResume(function() {
                console.log("录制恢复了")
            }), this.recorderManager.onError(function(t) {
                console.log("录制出错了:" + t), i.IsScreenRecord = !1
            })), this.recorderManager.start({
                duration: 30
            })) : "baidu" == this.platform && (null == this.recorderManager && (this.recorderManager = swan.getVideoRecorderManager(), this.recorderManager.onStart(function(t) {
                console.log("录制开始了: " + t)
            }), this.recorderManager.onStop(function(t) {
                console.log("录制结束了: " + t.videoPath), i.IsScreenRecord = !1, i.videoPath = t.videoPath, cc.director.emit("ScreenOver", {}), i.ShareRecordScreen()
            }), this.recorderManager.onPause(function() {
                console.log("录制暂停了")
            }), this.recorderManager.onResume(function() {
                console.log("录制恢复了")
            }), this.recorderManager.onError(function(t) {
                console.log("录制出错了:" + t), i.IsScreenRecord = !1
            })), this.recorderManager.start({
                duration: 30,
                microphoneEnabled: !0
            }))
        },
        StopRecordScreen: function() {
            var t = !(arguments.length > 0 && void 0 !== arguments[0]) || arguments[0];
            if (this.IsScreenRecord = !1, this.overShare = t, "tt" == this.platform) {
                if (null == this.recorderManager) return;
                console.log("停止录制"), this.recorderManager.stop()
            } else if ("baidu" == this.platform) {
                if (null == this.recorderManager) return;
                console.log("停止录制"), this.recorderManager.stop()
            }
        },
        ShareRecordScreen: function() {
            if (cc.Mgr.Utils.GetSysTime() - cc.Mgr.Global.screenStartTime <= 3) return cc.Mgr.Global.screenStartTime = 0, void this.showToast("录制时间少于三秒,未能成功保存视频");
            if ("tt" == this.platform) {
                if (null == this.recorderManager || "" == this.videoPath) return;
                1 == this.overShare && this.showToast("结束录屏"), console.log("分享录制的视频"), tt.shareVideo({
                    videoPath: "" + this.videoPath,
                    success: function() {
                        i.showToast("分享录制视频成功！")
                    },
                    fail: function(t) {
                        i.showToast("分享录制视频失败！")
                    }
                })
            } else if ("baidu" == this.platform) {
                if (null == this.recorderManager || null == this.videoPath) return;
                console.log("分享录制的视频"), swan.shareVideo({
                    videoPath: "" + this.videoPath,
                    success: function() {
                        console.log("分享成功！")
                    },
                    fail: function(t) {
                        console.log("分享失败！" + t)
                    }
                })
            }
        },
        JumpToOtherApp: function(t) {
            "wx" == this.platform ? wx.navigateToMiniProgram({
                appId: t,
                envVersion: "release",
                success: function(t) {
                    console.log("成功打开了其他小程序")
                }
            }) : "baidu" == this.platform ? swan.navigateToMiniProgram({
                appKey: t,
                extraData: {},
                success: function(t) {
                    console.log("百度跳转 success", t)
                },
                fail: function(t) {
                    console.log("百度跳转 fail", t)
                }
            }) : "tt" == this.platform && tt.navigateToMiniProgram({
                appId: t,
                extraData: {},
                success: function(t) {
                    console.log(" success", t)
                },
                fail: function(t) {
                    console.log(" fail", t)
                }
            })
        }
    }
});