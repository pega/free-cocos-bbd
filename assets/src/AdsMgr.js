let e = module


var i = require('AdsParam'),
    r = null,
    n = cc.Class({
        statics: {
            openAds: !0,
            platform: "pc",
            bannerId_baidu: "6305713",
            bannerId_wx: "adunit-66dedab9e8c70fcf",
            bannerId_tt: "1385k3f8p2615kn5eg",
            insertId_wx_1: "adunit-33f99d97c36d283d",
            insertId_wx_2: "adunit-a0e7adabb45d23e0",
            bdVideoId_1: "6305753",
            bdVideoId_2: "6305781",
            bdVideoId_3: "6305782",
            bdVideoId_4: "6305784",
            bdVideoId_5: "6305786",
            bdVideoId_6: "6305788",
            bdVideoId_7: "6305753",
            bdVideoId_8: "6305753",
            bdVideoId_9: "6305753",
            bdVideoId_10: "6305753",
            wxVideoId_1: "adunit-d71848c4c1c90696",
            wxVideoId_2: "adunit-a87f257ba82ed524",
            wxVideoId_3: "adunit-65cdacb418a56f73",
            wxVideoId_4: "adunit-65cdacb418a56f73",
            wxVideoId_5: "adunit-65cdacb418a56f73",
            wxVideoId_6: "adunit-65cdacb418a56f73",
            wxVideoId_7: "adunit-503baddcdc515459",
            wxVideoId_8: "adunit-503baddcdc515459",
            wxVideoId_9: "adunit-503baddcdc515459",
            wxVideoId_10: "adunit-503baddcdc515459",
            ttVideoId_1: "1e1bb9ab8d6cb2thjx",
            ttVideoId_2: "7egi1i702eh13hr5p6",
            ttVideoId_3: "104gb47an0f034m2rd",
            ttVideoId_4: "odwdtprweqh5k6l8hg",
            ttVideoId_5: "ecl27de0h6kbdkq4ql",
            ttVideoId_6: "ecl27de0h6kbdkq4ql",
            ttVideoId_7: "",
            ttVideoId_8: "",
            ttVideoId_9: "",
            ttVideoId_10: "",
            allScreen: !1,
            hasReSizeBanner: !1,
            resizeTime: 0,
            Init: function() {
                window.wx || window.tt || window.swan || (this.platform = "pc"), this.allScreen = !1, r = this
            },
            getBannerAdId: function() {
                return "wx" == this.platform ? this.bannerId_wx : "baidu" == this.platform ? this.bannerId_baidu : "tt" == this.platform ? this.bannerId_tt : null
            },
            getInsertAdId: function(t) {
                if ("wx" == this.platform) {
                    if (1 == t) return this.insertId_wx_1;
                    if (2 == t) return this.insertId_wx_2
                } else "baidu" == this.platform || this.platform;
                return null
            },
            getVideoAdId: function(t) {
                var e = "";
                if ("wx" == this.platform) switch (t) {
                    case i.PointA:
                        e = this.wxVideoId_1;
                        break;
                    case i.PointB:
                        e = this.wxVideoId_2;
                        break;
                    case i.PointC:
                        e = this.wxVideoId_3;
                        break;
                    case i.PointD:
                        e = this.wxVideoId_4;
                        break;
                    case i.PointE:
                        e = this.wxVideoId_5;
                        break;
                    case i.PointF:
                        e = this.wxVideoId_6;
                        break;
                    case i.PointG:
                        e = this.wxVideoId_7;
                        break;
                    case i.PointH:
                        e = this.wxVideoId_8;
                        break;
                    case i.PointI:
                        e = this.wxVideoId_9;
                        break;
                    case i.PointJ:
                        e = this.wxVideoId_10
                } else if ("baidu" == this.platform) switch (t) {
                    case i.PointA:
                        e = this.bdVideoId_1;
                        break;
                    case i.PointB:
                        e = this.bdVideoId_2;
                        break;
                    case i.PointC:
                        e = this.bdVideoId_3;
                        break;
                    case i.PointD:
                        e = this.bdVideoId_4;
                        break;
                    case i.PointE:
                        e = this.bdVideoId_5;
                        break;
                    case i.PointF:
                        e = this.bdVideoId_6;
                        break;
                    case i.PointG:
                        e = this.bdVideoId_7;
                        break;
                    case i.PointH:
                        e = this.bdVideoId_8;
                        break;
                    case i.PointI:
                        e = this.bdVideoId_9;
                        break;
                    case i.PointJ:
                        e = this.bdVideoId_10
                } else if ("tt" == this.platform) switch (t) {
                    case i.PointA:
                        e = this.ttVideoId_1;
                        break;
                    case i.PointB:
                        e = this.ttVideoId_2;
                        break;
                    case i.PointC:
                        e = this.ttVideoId_3;
                        break;
                    case i.PointD:
                        e = this.ttVideoId_4;
                        break;
                    case i.PointE:
                        e = this.ttVideoId_5;
                        break;
                    case i.PointF:
                        e = this.ttVideoId_6;
                        break;
                    case i.PointG:
                        e = this.ttVideoId_7;
                        break;
                    case i.PointH:
                        e = this.ttVideoId_8;
                        break;
                    case i.PointI:
                        e = this.ttVideoId_9;
                        break;
                    case i.PointJ:
                        e = this.ttVideoId_10
                }
                return e
            },
            ShowBannerAds: function() {
                if (0 != this.openAds)
                    if (this.resizeTime = 0, null == this.bannerAdShowNum && (this.bannerAdShowNum = 0), this.bannerAdShowNum++, "wx" == this.platform) {
                        if (null != this.bannerAdCtrl && this.bannerAdShowNum % 5 != 0) return;
                        this.bannerAdShowNum = 0, null != this.bannerAdCtrl && this.bannerAdCtrl.destroy();
                        var t = {};
                        t.W = wx.getSystemInfoSync().windowWidth, t.H = wx.getSystemInfoSync().windowHeight;
                        var e = 0;
                        e = this.allScreen ? t.W : 300, this.bannerAdCtrl = wx.createBannerAd({
                            adUnitId: r.getBannerAdId(),
                            style: {
                                left: 0,
                                top: 0,
                                width: e
                            }
                        }), this.bannerAdCtrl.onResize(function(e) {
                            console.log("重置广告宽度 " + t.H + " , " + r.bannerAdCtrl.style.realHeight), r.bannerAdCtrl.style.top = t.H - r.bannerAdCtrl.style.realHeight, r.bannerAdCtrl.style.left = (t.W - r.bannerAdCtrl.style.realWidth) / 2, r.bannerAdCtrl.show()
                        }), this.bannerAdCtrl.onError(function(t) {
                            console.log(t)
                        }), console.log("显示广告")
                    } else if ("baidu" == this.platform) {
                    if ("ios" == swan.getSystemInfoSync().platform) return;
                    null != this.bannerAdCtrl && this.bannerAdCtrl.destroy();
                    var o = {};
                    o.W = swan.getSystemInfoSync().windowWidth, o.H = swan.getSystemInfoSync().windowHeight, console.log("百度的 bannerId = " + this.getBannerAdId()), this.bannerAdCtrl = swan.createBannerAd({
                        adUnitId: r.getBannerAdId(),
                        appSid: "f06bda59",
                        style: {
                            left: 0,
                            top: 0,
                            width: 321
                        }
                    }), this.allScreen ? r.bannerAdCtrl.style.width = o.H : r.bannerAdCtrl.style.width = 300, this.bannerAdCtrl.onResize(function(t) {
                        console.log("重置广告宽度 "), r.bannerAdCtrl.style.top = o.H - t.height, r.bannerAdCtrl.style.left = (o.W - t.width) / 2, r.bannerAdCtrl.show()
                    }), this.bannerAdCtrl.onLoad(function() {
                        console.log(" banner 加载完成"), r.bannerAdCtrl.show()
                    }), this.bannerAdCtrl.onError(function(t) {
                        console.log(t)
                    }), console.log("显示广告")
                } else if ("tt" == this.platform) {
                    null != this.bannerAdCtrl && this.bannerAdCtrl.destroy();
                    var i = {};
                    i.W = tt.getSystemInfoSync().windowWidth, i.H = tt.getSystemInfoSync().windowHeight, console.log("头条 bannerId = " + this.getBannerAdId()), this.bannerAdCtrl = tt.createBannerAd({
                        adUnitId: this.getBannerAdId(),
                        style: {
                            left: (i.W - 200) / 2,
                            width: 200,
                            top: i.H - 112.5
                        }
                    }), this.bannerAdCtrl.onLoad(function() {
                        console.log(" banner 加载完成"), r.bannerAdCtrl.show()
                    }), this.bannerAdCtrl.onError(function(t) {
                        console.log(t)
                    }), console.log("显示广告")
                }
            },
            HideBannerAd: function() {
                console.log("隐藏banner"), null != this.bannerAdCtrl && this.bannerAdCtrl.hide(), this.DestroyBanner()
            },
            RecoverShowBanner: function() {
                null != this.bannerAdCtrl ? this.bannerAdCtrl.show() : this.ShowBannerAds()
            },
            DestroyBanner: function() {
                null != this.bannerAdCtrl && this.bannerAdCtrl.destroy(), this.bannerAdCtrl = null
            },
            ShowInsertAds: function(t) {
                if (0 != this.openAds && "wx" == this.platform) {
                    var e = wx.createInterstitialAd({
                        adUnitId: r.getInsertAdId(t)
                    });
                    e.show().catch(function(t) {
                        console.error(t)
                    }), e.onError(function(t) {
                        console.log(t)
                    }), e.onClose(function(t) {
                        console.log("插屏 广告关闭"), e.offLoad(), e.offError(), e.offClose()
                    })
                }
            },
            ShowVideoAds: function(t, e) {
                var o = this;
                if (this.cb = null, this.cb = e, 0 == this.openAds && this.platform, 0 != this.openAds)
                    if ("wx" == this.platform) {
                        var i = wx.createRewardedVideoAd({
                            adUnitId: r.getVideoAdId(t)
                        });
                        i.load().then(function() {
                            return i.show()
                        }).catch(function(t) {
                            return console.log(t.errMsg)
                        }), i.onError(function(t) {
                            console.log("广告加载出错 " + t.errMsg), o.cb(-1)
                        }), i.onLoad(function() {
                            console.log("加载事件回调")
                        }), i.onClose(function(t) {
                            console.log("是否观看完整了 " + t.isEnded), o.cb(t.isEnded ? 0 : 1), i.offLoad(), i.offError(), i.offClose()
                        })
                    } else if ("baidu" == this.platform) {
                    console.log("百度的视频广告  " + this.getVideoAdId(t));
                    var n = swan.createRewardedVideoAd({
                        adUnitId: r.getVideoAdId(t),
                        appSid: "f06bda59"
                    });
                    n.load().then(function() {
                        return n.show()
                    }).catch(function(t) {
                        return console.log(t.errMsg)
                    }), n.onError(function(t) {
                        console.log("广告加载出错 " + t.errMsg), o.cb(-1)
                    }), n.onLoad(function() {
                        console.log("加载事件回调")
                    }), n.onClose(function(t) {
                        console.log("是否观看完整了 " + t.isEnded), r.cb(t.isEnded ? 0 : 1), n.offLoad(), n.offError(), n.offClose()
                    })
                } else if ("tt" == this.platform) {
                    console.log("头条的视频广告  " + this.getVideoAdId(t));
                    var s = tt.createRewardedVideoAd({
                        adUnitId: r.getVideoAdId(t)
                    });
                    s.load().then(function() {
                        return s.show()
                    }).catch(function(t) {
                        return console.log(t.errMsg)
                    }), s.onError(function(t) {
                        console.log("广告加载出错 " + t.errMsg), o.cb(-1)
                    }), s.onLoad(function() {
                        console.log("头条视频加载事件回调")
                    }), s.onClose(function(t) {
                        console.log("是否观看完整了 " + t.isEnded), o.cb(t.isEnded ? 0 : 1), s.offLoad(), s.offError(), s.offClose()
                    })
                } else this.cb(0);
                else cc.Mgr.PlatformController.showToast("该功能暂未开放")
            }
        }
    });
e.exports = n