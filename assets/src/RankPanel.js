cc.Class({
    extends: cc.Component,
    properties: {},
    NextPage: function() {
        cc.Mgr.PlatformController.SendMessageToSubView("ToNextPage")
    },
    FrontPage: function() {
        cc.Mgr.PlatformController.SendMessageToSubView("ToFrontPage")
    },
    ClosePanel: function() {
        cc.Mgr.PlatformController.SendMessageToSubView("RankPanelCloseInMainView"), this.node.active = !1
    }
});