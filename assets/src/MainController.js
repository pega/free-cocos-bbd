var i;

function r(t, e, o) {
    return e in t ? Object.defineProperty(t, e, {
        value: o,
        enumerable: !0,
        configurable: !0,
        writable: !0
    }) : t[e] = o, t
}
var n = require('Ball'),
    s = require('Barrier'),
    c = require('Config'),
    a = require('Bomb'),
    l = require('BarrierType'),
    h = require('LvUpUI');

require('Shake');
cc.Class((r(i = {
    extends: cc.Component,
    properties: function() {
        return {
            barrierPres: [cc.Prefab],
            barrierPresBuff: [cc.Prefab],
            ballPre: cc.Prefab,
            bombPre: cc.Prefab,
            balls: [n],
            barriers: [s],
            scoreLbl: cc.Label,
            ballCountLbl: cc.Label,
            guide: cc.Node,
            gameStatus: !0,
            gameOverMark: cc.Node,
            takeAim: cc.Node,
            lessBallNode: cc.Node,
            upGradeNode: cc.Node,
            takeAimGraphics: cc.Graphics,
            ballParent: cc.Node,
            barrierParent: cc.Node,
            bombExploreParent: cc.Node,
            overScoreLbl: cc.Label,
            hasSubBall: !1,
            Hp: 3,
            starList: [cc.Node],
            pausePanel: cc.Node,
            pauseScoreLbl: cc.Label,
            pauseStartList: [cc.Node],
            hpList: [cc.Node],
            overPaticle: cc.ParticleSystem,
            flashLight: cc.Animation,
            useFlashLightUI: cc.Node,
            gameStep: 0,
            lvUpUI: h
        }
    },
    onLoad: function() {
        cc.director.getCollisionManager().enabled = !0, cc.director.getPhysicsManager().enabled = !0, cc.director.getActionManager().gravity = cc.v2(0, -1500), this.AddListener(), this.init(), this.initHpFull(), this.guideShow(), this.addBarriersAtBottom(), cc.Mgr.Global.sceneA = !1, this.overPaticle.node.active = !1
    },
    init: function() {
        cc.Mgr.Global.boomScale = 1, this.score = 0, this.recycleBallsCount = 1, this.barrierScoreRate = 0, this.balls[0].main = this, this.balls[0].node.group = c.groupBallInRecycle, this.gameOverMark.active = !1, this.gameOverMark.zIndex = 10, this.guide.zIndex = 10, this.guide.active = !1, this.takeAim.main = this, this.scoreLbl.string = "0", this.ballCountLbl.string = "1", this.gameStep = 0
    },
    SetPhysicsManager: function() {},
    AddListener: function() {
        this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchBeStart, this), this.node.on(cc.Node.EventType.TOUCH_END, this.onTouchIsEnd, this)
    },
    onTouchBeStart: function() {
        this.guideHide()
    },
    initHpFull: function() {
        this.Hp = 3;
        for (var t = 0; t < 3; t++) this.hpList[t].active = !0
    },
    SubPlayerHp: function() {
        this.Hp -= 1;
        for (var t = 0; t < 3; t++) this.hpList[t].active = !1;
        for (t = 0; t < this.Hp; t++) this.hpList[t].active = !0
    },
    AdsFullHp: function() {
        if (cc.Mgr.AudioMgr.playSFX("click"), 3 != this.Hp) {
            var t = this;
            cc.Mgr.AdsMgr.ShowVideoAds(3, function(e) {
                0 == e && t.initHpFull()
            })
        } else cc.Mgr.PlatformController.showToast("满血状态,无需回血")
    },
    onTouchIsEnd: function(t) {
        if (this.isRecycleBallsFinished()) {
            this.takeAimGraphics.clear(), this.recycleBallsCount = 0;
            var e = this.node.convertTouchToNodeSpaceAR(t.touch);
            this.lastTouchPos = e, this.shootAllBallsOut(e.sub(cc.v2(0, 420))), this.hasSubBall = !1
        }
    },
    upGradeBoom: function() {
        cc.Mgr.AudioMgr.playSFX("click"), 1.8 != cc.Mgr.Global.boomScale ? (this.upGradeNode.active = !0, cc.Mgr.Global.openAds && cc.Mgr.AdsMgr.RecoverShowBanner()) : cc.Mgr.PlatformController.showToast("已经升级到满级")
    },
    adsUpGradeBoom: function() {
        cc.Mgr.AudioMgr.playSFX("click");
        var t = this;
        cc.Mgr.AdsMgr.ShowVideoAds(4, function(e) {
            0 == e && (1 == cc.Mgr.Global.boomScale ? (cc.Mgr.Global.boomScale = 1.2, cc.Mgr.PlatformController.showToast("爆弹爆破范围升级到一级")) : 1.2 == cc.Mgr.Global.boomScale ? (cc.Mgr.PlatformController.showToast("爆弹爆破范围升级到二级"), cc.Mgr.Global.boomScale = 1.4) : 1.4 == cc.Mgr.Global.boomScale && (cc.Mgr.PlatformController.showToast("爆弹爆破范围升级到三级"), cc.Mgr.Global.boomScale = 1.5), t.CloseUpGradeBoom())
        })
    },
    CloseUpGradeBoom: function() {
        cc.Mgr.AudioMgr.playSFX("click"), this.upGradeNode.active = !1, cc.Mgr.AdsMgr.HideBannerAd()
    },
    addBallInGameIng: function(t) {
        var e = cc.instantiate(this.ballPre).getComponent(n);
        e.node.parent = this.ballParent, e.node.position = t;
        var o = cc.v2(t.x - 400, t.y + 400);
        t.x < 0 && (o = cc.v2(t.x + 400, t.y + 400));
        var i = o.sub(t);
        e.rigidBody.linearVelocity = i.mul(3), e.main = this, e.node.group = c.groupBallInGame, this.balls.push(e), this.setBallsCountLbl(this.balls.length)
    },
    addBallInGameIngFromCuty: function(t) {
        for (var e = [
                [400, 0],
                [280, 280],
                [0, 400],
                [-280, 280],
                [-400, 0],
                [-280, -280],
                [0, -400],
                [280, -280]
            ], o = 0; o < 8; o++) {
            var i = cc.instantiate(this.ballPre).getComponent(n);
            i.node.parent = this.ballParent, i.node.position = t;
            var r = cc.v2(t.x + e[o][0], t.y + e[o][1]).sub(t);
            i.rigidBody.linearVelocity = r.mul(3), i.main = this, i.node.group = c.groupBallInGame, this.balls.push(i), this.setBallsCountLbl(this.balls.length)
        }
    },
    hellSubBall: function(t) {
        var e = this.balls.indexOf(t);
        e > -1 && this.balls.splice(e, 1), t.node.destroy(), this.setBallsCountLbl(this.balls.length), this.hasSubBall = !0, 0 == this.balls.length && (this.lessBallNode.active = !0, cc.Mgr.Global.openAds && cc.Mgr.AdsMgr.RecoverShowBanner()), cc.log("拥有多少球 = " + this.balls.length)
    },
    onlyAddOneBallContinue: function() {
        cc.Mgr.AudioMgr.playSFX("click"), this.lessBallNode.active = !1, cc.Mgr.AdsMgr.HideBannerAd(), this.balls.length >= 1 || (this.unscheduleAllCallbacks(), this.schedule(function() {
            var t = cc.instantiate(this.ballPre).getComponent(n);
            t.node.parent = this.ballParent;
            var e = cc.v2(0, 440);
            t.node.position = e, t.main = this, t.node.group = c.groupBallInGame, this.balls.push(t), this.shootOutAdsGetBall(t), this.setBallsCountLbl(this.balls.length)
        }, .1, 2, .25))
    },
    addAdsGetFiveBall: function() {
        cc.Mgr.AudioMgr.playSFX("click");
        var t = this;
        cc.Mgr.AdsMgr.ShowVideoAds(5, function(e) {
            0 == e && (t.addBallsContinue(), cc.Mgr.PlatformController.showToast("获得十颗小球"))
        })
    },
    addBallsContinue: function() {
        cc.Mgr.AudioMgr.playSFX("click"), this.lessBallNode.active = !1, cc.Mgr.AdsMgr.HideBannerAd(), this.unscheduleAllCallbacks(), this.schedule(function() {
            var t = cc.instantiate(this.ballPre).getComponent(n);
            t.node.parent = this.ballParent;
            var e = cc.v2(0, 440);
            t.node.position = e, t.main = this, t.node.group = c.groupBallInGame, this.balls.push(t), this.shootOutAdsGetBall(t), this.setBallsCountLbl(this.balls.length)
        }, .1, 9, .25)
    },
    addAdsGetFiveBalls: function() {
        this.unscheduleAllCallbacks(), this.schedule(function() {
            var t = cc.instantiate(this.ballPre).getComponent(n);
            t.node.parent = this.ballParent;
            var e = cc.v2(0, 440);
            t.node.position = e, t.main = this, t.node.group = c.groupBallInGame, this.balls.push(t), this.shootOutAdsGetBall(t), this.setBallsCountLbl(this.balls.length)
        }, .1, 9, .25)
    },
    shootOutAdsGetBall: function(t) {
        null == this.lastTouchPos && (this.lastTouchPos = cc.v2(0, -200));
        var e = this.lastTouchPos.sub(cc.v2(0, 420));
        t.rigidBody.active = !1;
        var o = [];
        o.push(t.node.position), o.push(cc.v2(0, 420)), t.node.group = c.groupBallInGame, t.node.runAction(cc.sequence(cc.cardinalSplineTo(.8, o, .5), cc.callFunc(function() {
            cc.Mgr.AudioMgr.playSFX("fire"), t.rigidBody.active = !0, t.rigidBody.linearVelocity = e.mul(3)
        })))
    },
    AdsGetBall: function() {
        var t = this;
        cc.Mgr.AudioMgr.playSFX("click"), 0 != this.isRecycleBallsFinished() ? cc.Mgr.AdsMgr.ShowVideoAds(1, function(e) {
            0 == e && (t.addAdsGetFiveBalls(), cc.Mgr.PlatformController.showToast("获得十颗小球"))
        }) : cc.Mgr.PlatformController.showToast("请先等小球全部回收")
    },
    OpenPause: function() {
        cc.Mgr.AudioMgr.playSFX("click"), cc.Mgr.AdsMgr.ShowInsertAds(1), this.pausePanel.active = !0, this.gameStatus = !1, this.pauseScoreLbl.string = this.score.toString();
        for (var t = 0; t < 3; t++) this.pauseStartList[t].active = !1;
        var e = 0;
        this.score > 3e3 ? e = 3 : this.score > 1800 ? e = 2 : this.score > 800 && (e = 1);
        for (t = 0; t < e; t++) this.pauseStartList[t].active = !0
    },
    ClosePause: function() {
        cc.Mgr.AudioMgr.playSFX("click"), this.pausePanel.active = !1, this.gameStatus = !0
    },
    CheckGameOver: function() {
        this.pausePanel.active = !1, cc.Mgr.AdsMgr.ShowInsertAds(2), this.gameOver()
    },
    setBallsCountLbl: function(t) {
        this.ballCountLbl.string = t.toString()
    },
    openUseFlashLight: function() {
        cc.Mgr.AudioMgr.playSFX("click"), this.useFlashLightUI.active = !0
    },
    CloseUseFlashLight: function() {
        cc.Mgr.AudioMgr.playSFX("click"), this.useFlashLightUI.active = !1
    },
    useFlashLight: function() {
        cc.Mgr.AudioMgr.playSFX("click");
        var t = this;
        cc.Mgr.AdsMgr.ShowVideoAds(2, function(e) {
            0 == e && t.useFlashLightCallBack()
        })
    },
    useFlashLightCallBack: function() {
        this.useFlashLightUI.active = !1, this.scheduleOnce(function() {
            cc.Mgr.AudioMgr.playSFX("flash"), this.flashLight.node.active = !0, this.flashLight.play("flashLight")
        }, .35)
    }
}, "ClosePause", function() {
    cc.Mgr.AudioMgr.playSFX("click"), this.pausePanel.active = !1, this.gameStatus = !0
}), r(i, "shootOneBallOut", function(t, e) {
    t.rigidBody.active = !1;
    var o = [];
    o.push(t.node.position), o.push(cc.v2(0, 420)), t.node.group = c.groupBallInGame, t.node.runAction(cc.sequence(cc.cardinalSplineTo(.8, o, .5), cc.callFunc(function() {
        cc.Mgr.AudioMgr.playSFX("fire"), t.rigidBody.active = !0;
        var o = 3;
        e.mag() < 300 && (o = 5, cc.log("放大发射力度 = " + e.mag())), t.rigidBody.linearVelocity = e.mul(o)
    })))
}), r(i, "shootAllBallsOut", function(t) {
    var e = this;
    if (this.gameStatus)
        for (var o = function(o) {
                var i = e.balls[o];
                null != i && e.scheduleOnce(function() {
                    this.shootOneBallOut(i, t)
                }.bind(e), .1 * o)
            }, i = 0; i < this.balls.length; i++) o(i)
}), r(i, "recycleOneShootOutBall", function() {
    var t = this;
    if (this.recycleBallsCount++, this.barrierScoreRate += .2, this.isRecycleBallsFinished()) {
        for (var e = function(e) {
                var o = t.barriers[e];
                o.node.runAction(cc.sequence(cc.moveBy(.5, cc.v2(0, 100)), cc.callFunc(function() {
                    if (o.node.position.y > 200 && o.node.runAction(cc.shake(1.5, 3, 3)), o.node.position.y > 380) {
                        if (this.Hp > 1) o.bType != l.IsAddBall && this.SubPlayerHp(), cc.Mgr.PlatformController.QuakeScreen(!1);
                        else if (cc.Mgr.PlatformController.QuakeScreen(!0), o.bType != l.IsAddBall) return void this.gameOver();
                        this.removeOneBarrier(o)
                    }
                }.bind(t))))
            }, o = 0; o < this.barriers.length; o++) e(o);
        this.balls.length <= 3 && 1 == this.hasSubBall && (this.lessBallNode.active = !0, this.hasSubBall = !1, cc.Mgr.Global.openAds && cc.Mgr.AdsMgr.RecoverShowBanner()), this.addBarriersAtBottom()
    }
}), r(i, "isRecycleBallsFinished", function() {
    return this.recycleBallsCount == this.balls.length
}), r(i, "addBarriersAtBottom", function() {
    var t = -270 + this.getRandomSpace(),
        e = .82,
        o = .6,
        i = .82;
    for (0 == this.gameStep ? (e = .5, o = .15, i = .2) : 1 == this.gameStep && (e = .7, o = .4, i = .7), this.gameStep += 1; t < 270;) {
        if (Math.random() < e) {
            var r = cc.instantiate(this.barrierPres[Math.floor(Math.random() * this.barrierPres.length)]).getComponent(s);
            r.node.parent = this.barrierParent, r.node.position = cc.v2(t, -320), r.lbScore && (r.node.rotation = 360 * Math.random()), r.main = this, t += this.getRandomSpace(), this.barriers.push(r)
        } else {
            var n = Math.random();
            if (n < o) {
                var c = cc.instantiate(this.barrierPresBuff[0]).getComponent(s);
                c.node.parent = this.barrierParent, c.node.position = cc.v2(t, -320), c.lbScore && (c.node.rotation = 360 * Math.random()), c.main = this, t += this.getRandomSpace(), this.barriers.push(c)
            } else if (n < i) {
                var a = cc.instantiate(this.barrierPresBuff[1]).getComponent(s);
                a.node.parent = this.barrierParent, a.node.position = cc.v2(t, -320), a.lbScore && (a.node.rotation = 360 * Math.random()), a.main = this, t += this.getRandomSpace(), this.barriers.push(a)
            } else {
                var l = cc.instantiate(this.barrierPresBuff[2]).getComponent(s);
                l.node.parent = this.barrierParent, l.node.position = cc.v2(t, -320), l.lbScore && (l.node.rotation = 360 * Math.random()), l.main = this, t += this.getRandomSpace(), this.barriers.push(l)
            }
        }
    }
}), r(i, "addBombExploreAtPos", function(t) {
    var e = cc.instantiate(this.bombPre).getComponent(a);
    e.node.parent = this.bombExploreParent, e.node.group = c.bomb, e.node.position = t, e.main = this, e.PlayAni()
}), r(i, "shake", function(t) {
    var e = cc.shake(.7, 1, 1);
    t.node.runAction(e)
}), r(i, "addScore", function() {
    this.score++, this.scoreLbl.string = this.score.toString()
}), r(i, "addNumScore", function(t) {
    null == t && (t = 0), this.score += t, this.scoreLbl.string = this.score.toString()
}), r(i, "setBarrierScore", function() {
    return Math.floor(this.randomNum(1 + 2 * this.barrierScoreRate, 5 + 3 * this.barrierScoreRate))
}), r(i, "JumpToApp", function(t, e) {
    cc.Mgr.PlatformController.JumpToOtherApp(e)
}), r(i, "removeOneBarrier", function(t) {
    var e = this.barriers.indexOf(t);
    e > -1 && (t.node.removeFromParent(!1), this.barriers.splice(e, 1))
}), r(i, "getRandomSpace", function() {
    return 80 + 200 * Math.random()
}), r(i, "randomNum", function(t, e) {
    var o = e - t,
        i = Math.random();
    return t + Math.floor(i * o)
}), r(i, "guideShow", function() {
    this.guide.active = !0, this.guide.getChildByName("handMove").getComponent(cc.Animation).play("handMove")
}), r(i, "guideHide", function() {
    this.guide.active = !1, this.guide.getChildByName("handMove").getComponent(cc.Animation).stop("handMove")
}), r(i, "ReGame", function() {}), r(i, "AdsReStart", function() {
    this.gameStep = 0, cc.Mgr.AudioMgr.playSFX("click");
    var t = this;
    cc.Mgr.AdsMgr.ShowVideoAds(2, function(e) {
        0 == e && (cc.Mgr.AdsMgr.HideBannerAd(), t.useFlashLightCallBack(), t.ResetStatus(), t.gameOverMark.active = !1, t.overPaticle.node.active = !1, t.initHpFull())
    })
}), r(i, "onlyReStart", function() {
    cc.Mgr.AdsMgr.HideBannerAd(), this.gameStep = 0, cc.director.loadScene("game")
}), r(i, "ResetStatus", function() {
    this.gameStatus = !0
}), r(i, "RemoveTopBarrier", function() {
    cc.Mgr.PlatformController.QuakeScreen(!0);
    for (var t = 0; t < this.barriers.length; t++) {
        var e = this.barriers[t];
        e.node.position.y > 50 && this.removeOneBarrier(e)
    }
}), r(i, "gameOver", function() {
    cc.Mgr.Global.boomScale = 1, cc.Mgr.AudioMgr.playSFX("lose"), this.gameStatus = !1, this.gameOverMark.active = !0, cc.Mgr.Global.openAds && cc.Mgr.AdsMgr.RecoverShowBanner(), this.overScoreLbl.string = this.score.toString();
    for (var t = 0; t < 3; t++) this.starList[t].active = !1;
    var e = 0;
    this.score > 3e3 ? e = 3 : this.score > 1800 ? e = 2 : this.score > 800 && (e = 1);
    for (t = 0; t < e; t++) this.starList[t].active = !0;
    this.score > cc.Mgr.UserData.highScore && (this.overPaticle.node.active = !0, this.overPaticle.resetSystem(), cc.Mgr.UserData.highScore = this.score, cc.Mgr.UserData.SaveData(), cc.Mgr.PlatformController.setUserCloudStorage(cc.Mgr.UserData.getHisTopScore()))
}), r(i, "backHome", function() {
    cc.Mgr.Global.boomScale = 1, cc.Mgr.AdsMgr.ShowInsertAds(1), cc.director.loadScene("home")
}), r(i, "ShareGameResult", function() {
    cc.Mgr.AudioMgr.playSFX("click");
    var t = this.score + "分！你敢相信,快来迎战吧";
    cc.Mgr.PlatformController.ShareToFriendTxt(t)
}), i));